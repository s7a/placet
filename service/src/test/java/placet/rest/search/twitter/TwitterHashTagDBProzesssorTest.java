package placet.rest.search.twitter;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.twitter.hashtag.IHashtagDAO;
import placet.entity.info.twitter.HashTag;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProzessorWrapper;
import placet.util.PlacetUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/service_context_test.xml")
public class TwitterHashTagDBProzesssorTest
{
	@Autowired
	@Qualifier("DB")
	ITwitterHashTagProzessorWrapper hashTagProzessorWrapper;

	@Autowired
	IHashtagDAO hashTagDao;

	@Test
	public void isDataInDBTest()
	{
		final List<HashTag> hashTagsFormDB = hashTagDao.findAll();
		Assert.assertNotNull(hashTagsFormDB);
		Assert.assertEquals(10, hashTagsFormDB.size());
	}

	@Test
	@Transactional
	public void fetchCachedDataFromDBFresh()
	{
		// Dublin_IE, cached in DB
		List<HashTag> hashTagDbList;
		try
		{
			hashTagDbList = hashTagProzessorWrapper.fetchMostRelevantHashTags("", 53.3498053, -6.260309699999993, 10,
					RadiusType.KM, "en", ResultType.RECENT, 10, "Dublin", "127.0.0.1");
			Assert.assertNotNull(hashTagDbList);
			Assert.assertEquals(10, hashTagDbList.size());
		}
		catch (final HashTagProzessotException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test /* (timeout = 10000) */
	@Transactional
	public void fetchCachedDataFromDBOld() throws InterruptedException
	{
		// Berlin
		final HashTag oldHashTag = new HashTag();
		oldHashTag.setLatitude(52.5167);
		oldHashTag.setLongitude(13.3833);
		oldHashTag.setName("Junit");
		oldHashTag.setOccurrence(66);
		final LocalDateTime nowMinusDelta = LocalDateTime.now().minusMinutes(300);

		oldHashTag.setCreatedAt(PlacetUtils.convertLdtToDate(nowMinusDelta));
		hashTagDao.create(oldHashTag);

		List<HashTag> hashTagDbList;
		try
		{
			hashTagDbList = hashTagProzessorWrapper.fetchMostRelevantHashTags("", 52.5167, 13.3833, 10, RadiusType.KM,
					"en", ResultType.RECENT, 10, "Berlin", "127.0.0.1");
			Assert.assertNotNull(hashTagDbList.stream().filter(h -> h.getName().equals("Junit")).findAny());
			boolean isDBupdatred = false;
			while (!isDBupdatred)
			{
				hashTagDbList = hashTagDao.findAll();
				final long occurence = hashTagDbList.stream()
						.filter(h -> h.getLatitude() == 52.5167 && h.getLongitude() == 13.3833).count();
				if (occurence > 1)
				{
					isDBupdatred = true;
				}
				Thread.sleep(10000);
			}

		}
		catch (final HashTagProzessotException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	@Transactional
	public void fetchDataFromRestEndpoint()
	{
		// Trier_DE, not cached
		List<HashTag> hashTagEndpointList;
		try
		{
			hashTagEndpointList = hashTagProzessorWrapper.fetchMostRelevantHashTags("", 49.75, 6.6333333, 10,
					RadiusType.KM, "en", ResultType.RECENT, 10, "Katowice", "127.0.0.1");
			Assert.assertNotNull(hashTagEndpointList);
			Assert.assertEquals(10, hashTagEndpointList.size());
		}
		catch (final HashTagProzessotException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
