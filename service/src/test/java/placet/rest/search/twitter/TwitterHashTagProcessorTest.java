package placet.rest.search.twitter;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import placet.entity.info.twitter.HashTag;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/service_context_test.xml")
public class TwitterHashTagProcessorTest {

	@Autowired
	@Qualifier("endpoint")
	ITwitterHashTagProcessor hashTagProzessor;

	@Test
	@Transactional
	public void realEndpointTestRunForRecent() throws HashTagProzessotException {
		final List<HashTag> res = hashTagProzessor.fetchMostRelevantHashTags("", 53.3498053, -6.260309699999993, 10,
				RadiusType.KM, "en", ResultType.RECENT, 3, "Dublin", "127.0.0.1");

		Assert.assertNotNull(res);
		Assert.assertTrue(res.size() == 3);

	}

}
