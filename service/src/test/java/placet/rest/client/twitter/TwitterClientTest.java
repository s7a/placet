package placet.rest.client.twitter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.junit.Assert;
import placet.twitter.client.ITwitterClient;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.RestSearchRequestException;
import placet.twitter.client.rest.ResultType;
import placet.twitter.client.rest.authentication.AuthenticationException;
import placet.twitter.client.rest.response.SearchResponse;

/**
 * @author SynowiecA
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/service_context_test.xml")
public class TwitterClientTest
{
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Autowired
	ITwitterClient client;

	@Before
	public void setUp() throws Exception
	{
	}

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{

	}

	@Test
	public void restClientSuccess()
	{
		try
		{
			final SearchResponse res = client.getTweetsByGeoCode("", 53.3498053, -6.260309699999993, 10, RadiusType.KM,
					"en", ResultType.RECENT, -1, -1, 100);
			Assert.assertNotNull(res.getTweetList().stream()
					.filter(tweet -> tweet.getCreatedAtString() != null && tweet.getSource() != null
							&& tweet.getId() != null && tweet.getUser() != null && tweet.getText() != null)
					.findAny().isPresent());
		}
		catch (final RestSearchRequestException e)
		{
			exception.expect(AuthenticationException.class);
		}
	}

}
