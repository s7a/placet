package placet.rest.authentication.twitter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import placet.twitter.client.rest.authentication.AuthenticationException;
import placet.twitter.client.rest.authentication.TwitterOAuthBearerTokenBroker;

public class TwitterOAuthBearerTokenBrokerTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testOAuthBroker() {
		try {
			final String bearerToken = TwitterOAuthBearerTokenBroker.requestSecurityToken();
			assertNotNull(bearerToken);

		} catch (final AuthenticationException e) {
			exception.expect(AuthenticationException.class);

		}

	}
}
