package placet.twitter.search;

import java.util.List;

import placet.entity.info.twitter.HashTag;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;

public interface ITwitterHashTagProcessor {

	List<HashTag> fetchMostRelevantHashTags(String searchContent, double latitude, double longitude, double radius,
			RadiusType rdType, String lang, ResultType resType, int releventHashTagCount, String city, String ip)
					throws HashTagProzessotException;

}
