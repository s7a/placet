package placet.twitter.search.endpoint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import placet.dao.request.IRequestHistoryDAO;
import placet.entity.info.twitter.HashTag;
import placet.entity.info.twitter.Tweet;
import placet.entity.request.RequestHistory;
import placet.twitter.client.ITwitterClient;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.RestSearchRequestException;
import placet.twitter.client.rest.ResultType;
import placet.twitter.client.rest.response.HashTagRest;
import placet.twitter.client.rest.response.SearchResponse;
import placet.twitter.client.rest.response.TweetRest;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProcessor;
import placet.util.PlacetProperties;
import placet.util.PlacetUtils;

@Service
@Qualifier("endpoint")
public class TwitterHashTagProzessor implements ITwitterHashTagProcessor {

	private static Logger logger = LogManager.getLogger(TwitterHashTagProzessor.class.getName());
	private static final String SERVED_BY_ENDPOINT = "EP";
	private final ITwitterClient client;
	private final IRequestHistoryDAO requestHistDao;

	@Autowired
	public TwitterHashTagProzessor(ITwitterClient client, IRequestHistoryDAO requestHistDao) {
		this.client = client;
		this.requestHistDao = requestHistDao;
	}

	@Override
	public List<HashTag> fetchMostRelevantHashTags(String searchContent, double latitude, double longitude,
			double radius, RadiusType rdType, String lang, ResultType resType, int releventHashTagCount, String city,
			String ip) throws HashTagProzessotException {
		logger.debug("Start fetching and analysing hashTags from endpoint");
		final long startFetchAndAnalyse = System.currentTimeMillis();

		List<HashTag> analyzedHashTagList = new ArrayList<>();
		final List<TweetRest> tweets = new ArrayList<>();
		long maxId = -1;
		int returnCount = 0;
		SearchResponse response;
		final long startFetchingFromEndpoint = System.currentTimeMillis();

		try {

			for (int i = 0; i < PlacetProperties.getTweetReqestRound(); i++) {
				// TODO => Maybe different request strategies depending on the
				// passed resType?
				response = client.getTweetsByGeoCode(searchContent, latitude, longitude, radius, rdType, lang, resType,
						-1, maxId, 100);
				tweets.addAll(response.getTweetList());

				maxId = calculateNewMaxId(tweets);

			}
			final long endFetchingFromEndpoint = System.currentTimeMillis();
			logger.debug("Tweets fetched => " + tweets.size() + " in "
					+ (endFetchingFromEndpoint - startFetchingFromEndpoint) + " millisconds");

			analyzedHashTagList = analyseHashTags(tweets, latitude, longitude, releventHashTagCount);
		} catch (final RestSearchRequestException e) {
			throw new HashTagProzessotException("Could not process request ", e);
		}

		if (analyzedHashTagList.size() < releventHashTagCount) {
			logger.debug("The size [" + analyzedHashTagList.size()
					+ "] of the retrieved hashTagList is smaller than the requested relevantHashTagCount["
					+ releventHashTagCount + "]");
			returnCount = analyzedHashTagList.size();
		} else {
			returnCount = releventHashTagCount;
		}
		logger.debug("End fetching and analysing hashTags from endpoint");
		final long endFetchAndAnalyse = System.currentTimeMillis();

		logger.debug("Writing RequestHistory");
		final RequestHistory hist = new RequestHistory();
		hist.setCity(city);
		hist.setDate(new Date());
		hist.setIp(ip);
		hist.setResponseTime(endFetchAndAnalyse - startFetchAndAnalyse);
		hist.setServedBy(SERVED_BY_ENDPOINT);
		requestHistDao.create(hist);

		return analyzedHashTagList.subList(0, returnCount);
	}

	private long calculateNewMaxId(final List<TweetRest> tweets) {

		long idCounter;
		final LongSummaryStatistics statistics = tweets.stream().mapToLong(TweetRest::getId).summaryStatistics();
		idCounter = statistics.getMin();
		logger.info("New calculated min requestID =>" + idCounter);
		return idCounter;
	}

	private List<HashTag> analyseHashTags(List<TweetRest> tweets, double latitude, double longitude, int hashTagCount) {
		logger.debug("Start analysing Tweets");
		final long start = System.currentTimeMillis();
		final Map<String, Integer> rankedHashTags = sortMapByValue(rankHashTagPopulartiy(tweets));
		final long end = System.currentTimeMillis();
		final List<HashTag> hashTagList = mapRestResponseToEntityModel(tweets, rankedHashTags);
		hashTagList.stream().forEach(e -> {
			e.setLatitude(latitude);
			e.setLongitude(longitude);
		});
		logger.debug("End Analysing tweets, ranked HashTags computed in " + (end - start));
		return hashTagList;

	}

	private List<HashTag> mapRestResponseToEntityModel(List<TweetRest> tweets, Map<String, Integer> rangedHashMap) {

		logger.debug("Start mapping rest response to entity model");
		final long start = System.currentTimeMillis();
		final List<HashTag> hashTagList = new ArrayList<>();
		for (final Map.Entry<String, Integer> entry : rangedHashMap.entrySet()) {
			final HashTag hashTag = new HashTag();
			hashTag.setName(entry.getKey());
			hashTag.setOccurrence(entry.getValue());
			hashTag.setCreatedAt(new Date());
			final List<Tweet> tweetList = new ArrayList<>();
			for (final TweetRest restTweet : tweets) {
				final List<HashTagRest> hashTagRestList = restTweet.getEntities().getHashTags();
				for (final HashTagRest hashTagRest : hashTagRestList) {
					if (hashTagRest.getText().equalsIgnoreCase(entry.getKey())) {
						final Tweet tweet = new Tweet();

						tweet.setProfileImageUrl(restTweet.getUser().getProfileImageUrl());
						tweet.setTextContent(restTweet.getText());
						tweet.setTwitterID(restTweet.getId());
						tweet.setUserName(restTweet.getUser().getName());
						tweet.setScreenName(restTweet.getUser().getScreenName());
						tweet.setFavouritesCount(restTweet.getFavouritesCount());
						tweet.setRetweetCount(restTweet.getRetweetCount());
						tweet.setCreatedAt(getDateFromTwitterStringRep(restTweet.getCreatedAtString()));
						tweetList.add(tweet);
					}
				}

			}
			sortTweetsByDate(tweetList);
			// TODO => Maybe we should save all Tweets and and filter later in
			// the controller what we want to present !!
			// Actually this is what needs to be done once we run on own
			// hardware
			// This solution is only chosen because of the limitation of free
			// cloud account
			// !! A profile hock would be a nice solution !!
			hashTag.setTweets(PlacetUtils.getSubList(tweetList, PlacetProperties.getSavedTweetsPerHashTagNumber()));
			hashTagList.add(hashTag);
		}

		final long end = System.currentTimeMillis();
		logger.debug("Mappend rest response to entity model in " + (end - start));

		return hashTagList;
	}

	private Map<String, Integer> rankHashTagPopulartiy(List<TweetRest> tweets) {
		final Map<String, Integer> hashTagOccurrenceMap = new HashMap<>();
		for (final TweetRest tweet : tweets) {
			final List<HashTagRest> hashTagList = tweet.getEntities().getHashTags();
			for (final HashTagRest hashTag : hashTagList) {
				final String text = hashTag.getText().toLowerCase();
				final Set<String> alreadyIncrementet = new HashSet<>();
				if (!alreadyIncrementet.contains(text)) {
					if (hashTagOccurrenceMap.containsKey(text)) {
						Integer count = hashTagOccurrenceMap.get(text);
						hashTagOccurrenceMap.put(text, ++count);
						alreadyIncrementet.add(text);

					} else {
						hashTagOccurrenceMap.put(text, 1);
					}
				}
			}
		}
		return hashTagOccurrenceMap;

	}

	private Map<String, Integer> sortMapByValue(Map<String, Integer> unsortedMap) {

		final LinkedHashMap<String, Integer> sortedMap = unsortedMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> {
					throw new AssertionError();
				} , LinkedHashMap::new));

		return sortedMap;
	}

	private Date getDateFromTwitterStringRep(String dateInStringFormat) {
		final DateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy", Locale.ENGLISH);
		Date date = null;
		try {
			date = format.parse(dateInStringFormat);
		} catch (final ParseException e) {
			logger.debug("Date String couldn't be converted to date object " + e);
		}
		return date;

	}

	private void sortTweetsByDate(List<Tweet> tweetList) {
		Collections.sort(tweetList, new Comparator<Tweet>() {
			@Override
			public int compare(Tweet o1, Tweet o2) {
				return o2.getCreatedAt().compareTo(o1.getCreatedAt());
			}
		});

	}

	private void sortTweetsByPopularity(List<Tweet> tweetList) {
		// TODO => ...
	}

}
