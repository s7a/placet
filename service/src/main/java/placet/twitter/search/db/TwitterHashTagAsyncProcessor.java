package placet.twitter.search.db;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.twitter.hashtag.IHashtagDAO;
import placet.entity.info.twitter.HashTag;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProcessor;
import placet.twitter.search.ITwitterHashTagProzessorWrapper;

// TODO => Hmmm, maybe we should declare a separate interface for this
@Service
@Qualifier("AsyncDB")
public class TwitterHashTagAsyncProcessor implements ITwitterHashTagProzessorWrapper
{
	private static Logger logger = LogManager.getLogger(TwitterHashTagAsyncProcessor.class.getName());

	ITwitterHashTagProcessor endpointProcessor;

	IHashtagDAO hashTagDao;

	@Autowired
	public TwitterHashTagAsyncProcessor(@Qualifier("endpoint") ITwitterHashTagProcessor endpointProcessor,
			IHashtagDAO hashTagDao)
	{
		this.endpointProcessor = endpointProcessor;
		this.hashTagDao = hashTagDao;
	}

	@Override
	@Async
	// @Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<HashTag> fetchMostRelevantHashTags(String searchContent, double latitude, double longitude,
			double radius, RadiusType rdType, String lang, ResultType resType, int releventHashTagCount, String city,
			String ip) throws HashTagProzessotException
	{
		logger.debug("Calling Twitter endpoint asynchronously");
		List<HashTag> reFreshHashTagsFromEndpoint = null;
		try
		{
			reFreshHashTagsFromEndpoint = endpointProcessor.fetchMostRelevantHashTags(searchContent, latitude,
					longitude, radius, rdType, lang, resType, releventHashTagCount, city, ip);
			hashTagDao.batchInsert(reFreshHashTagsFromEndpoint);
		}
		catch (final HashTagProzessotException e)
		{
			logger.debug("Async update of HashTag failed ", e);
		}

		logger.debug(reFreshHashTagsFromEndpoint.size() + "HashTags updated in DB");

		return reFreshHashTagsFromEndpoint;

	}

}
