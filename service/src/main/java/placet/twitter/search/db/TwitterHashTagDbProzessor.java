package placet.twitter.search.db;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.request.IRequestHistoryDAO;
import placet.dao.twitter.hashtag.IHashtagDAO;
import placet.entity.info.twitter.HashTag;
import placet.entity.request.RequestHistory;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProcessor;
import placet.twitter.search.ITwitterHashTagProzessorWrapper;
import placet.util.PlacetProperties;
import placet.util.PlacetUtils;

@Service
@Transactional
@Qualifier("DB")
public class TwitterHashTagDbProzessor implements ITwitterHashTagProzessorWrapper
{

	private static Logger logger = LogManager.getLogger(TwitterHashTagDbProzessor.class.getName());

	private static final String SERVED_BY_DB_FRESH = "DB_FRESH";
	private static final String SERVED_BY_DB_OLD = "DB_OlD";
	private static final String SERVED_BY_ENDPOINT = "EP";
	private ITwitterHashTagProcessor hashTagProcessor;
	private final IHashtagDAO hashTagDao;
	private final IRequestHistoryDAO requestHistDao;
	private ITwitterHashTagProzessorWrapper asyncProcessor;

	@Autowired
	public TwitterHashTagDbProzessor(IHashtagDAO hashTagDao, IRequestHistoryDAO requestHistDAO)
	{
		this.hashTagDao = hashTagDao;
		this.requestHistDao = requestHistDAO;
	}

	@Override
	public List<HashTag> fetchMostRelevantHashTags(String searchContent, double latitude, double longitude,
			double radius, RadiusType rdType, String lang, ResultType resType, int releventHashTagCount, String city,
			String ip) throws HashTagProzessotException
	{
		final int tweetReqestRound = PlacetProperties.getTweetReqestRound();
		logger.debug("Start fetching HashTags witch patameter searchContent[" + searchContent + "] latidude[" + latitude
				+ "] longitude[" + longitude + "] radius[" + radius + "] rdType[" + rdType + "] lang[" + lang
				+ "] ResultType[" + resType + "] tweetRequestRound[" + tweetReqestRound + "] releventHashTagCount["
				+ releventHashTagCount + "] [" + city + "] [" + ip + "]");

		final long cacheFreshTimeout = PlacetProperties.getCacheFreshTimeout();
		final long cacheOldTimeout = PlacetProperties.getCacheOldTimeout();
		// First we try to obtain results cached in the DB [the hitrate
		// is
		// heavily dependent on the timeout we allow]

		logger.debug("Trying to hit the DB for request latitude[" + latitude + "] longitude[" + longitude
				+ "] freshChacheTimeout[" + cacheFreshTimeout + "]");
		long startDB = System.currentTimeMillis();
		List<HashTag> hashTags = getHashTagsFromDbUsingCacheTimeout(latitude, longitude, cacheFreshTimeout);
		long endDB = System.currentTimeMillis();

		if (hashTags == null || hashTags.isEmpty())
		{
			logger.debug("No recent cached HashTags found, trying to retrieve from DB with cacheOldTimeout["
					+ cacheOldTimeout + "]");
			startDB = System.currentTimeMillis();
			hashTags = getHashTagsFromDbUsingCacheTimeout(latitude, longitude, cacheOldTimeout);
			endDB = System.currentTimeMillis();
			if (hashTags == null || hashTags.isEmpty())
			{

				logger.debug("No luck calling DB for cached entities, need to call REST twitter endpoint");
				hashTags = hashTagProcessor.fetchMostRelevantHashTags(searchContent, latitude, longitude, radius,
						rdType, lang, resType, releventHashTagCount, city, ip);
				hashTagDao.batchInsert(hashTags);
				writeRequestHistory(hashTags, city, ip, startDB, endDB, SERVED_BY_ENDPOINT);

			}
			else
			{
				asyncProcessor.fetchMostRelevantHashTags(searchContent, latitude, longitude, radius, rdType, lang,
						resType, releventHashTagCount, city, ip);
				writeRequestHistory(hashTags, city, ip, startDB, endDB, SERVED_BY_DB_OLD);

			}
		}
		else
		{
			writeRequestHistory(hashTags, city, ip, startDB, endDB, SERVED_BY_DB_FRESH);
		}

		return PlacetUtils.getSubList(hashTags, releventHashTagCount);
	}

	private List<HashTag> getHashTagsFromDbUsingCacheTimeout(double latitude, double longitude,
			final long cacheTimoutInMin)
	{
		final LocalDateTime nowMinusDelta = LocalDateTime.now().minusMinutes(cacheTimoutInMin);
		final List<HashTag> hashTags = hashTagDao.findByGeoLocationAndDateTime(longitude, latitude,
				PlacetUtils.convertLdtToDate((nowMinusDelta)));
		return hashTags;
	}

	private void writeRequestHistory(List<HashTag> hashTags, String city, String ip, final long startDB,
			final long endDB, String servedBy)
	{
		logger.debug("Found valid entities in the DB => HashTagCount[" + hashTags.size() + "] creating RequestHistory");
		final RequestHistory hist = new RequestHistory();
		hist.setDate(new Date());
		hist.setResponseTime(endDB - startDB);
		hist.setCity(city);
		hist.setIp(ip);
		hist.setServedBy(servedBy);
		requestHistDao.create(hist);
	}

	public ITwitterHashTagProcessor getHashTagProcessor()
	{
		return hashTagProcessor;
	}

	@Autowired
	@Qualifier("endpoint")
	public void setHashTagProcessor(ITwitterHashTagProcessor hashTagProcessor)
	{
		this.hashTagProcessor = hashTagProcessor;
	}

	public ITwitterHashTagProzessorWrapper getAsyncProcessor()
	{
		return asyncProcessor;
	}

	@Autowired
	@Qualifier("AsyncDB")
	public void setAsyncProcessor(ITwitterHashTagProzessorWrapper asyncProcessor)
	{
		this.asyncProcessor = asyncProcessor;
	}

}
