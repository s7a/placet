package placet.twitter.search;

public class HashTagProzessotException extends Exception
{
	public HashTagProzessotException()
	{
	}

	public HashTagProzessotException(String message)
	{
		super(message);
	}

	public HashTagProzessotException(Throwable cause)
	{
		super(cause);
	}

	public HashTagProzessotException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public HashTagProzessotException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
