package placet.twitter.client.rest;

public class RestSearchRequestException extends Exception
{

	public RestSearchRequestException()
	{
	}

	public RestSearchRequestException(String message)
	{
		super(message);
	}

	public RestSearchRequestException(Throwable cause)
	{
		super(cause);
	}

	public RestSearchRequestException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public RestSearchRequestException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
