package placet.twitter.client.rest.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Arturooo
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class HashTagRest {

	@XmlElement(name = "text")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
