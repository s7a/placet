package placet.twitter.client.rest;

import static placet.twitter.client.rest.RestClientConstants.JASON_SEARCH_TWEETS_PATH;
import static placet.twitter.client.rest.RestClientConstants.TWITTER_ENDPOINT;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import placet.twitter.client.ITwitterClient;
import placet.twitter.client.rest.authentication.AuthenticationException;
import placet.twitter.client.rest.authentication.TwitterOAuthBearerTokenBroker;
import placet.twitter.client.rest.response.SearchResponse;

/**
 * @author SynowiecA
 *
 */
@Component
public class TwitterRestClient implements ITwitterClient
{

	private final static Logger logger = LogManager.getLogger(TwitterRestClient.class.getName());

	private final ClientConfig clientConfig;
	private final Client client;
	private final WebResource twitterEndpoint;

	public TwitterRestClient()
	{

		clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);
		client.addFilter(new LoggingFilter());
		twitterEndpoint = client.resource(TWITTER_ENDPOINT + JASON_SEARCH_TWEETS_PATH);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see placet.twitter.client.ITwitterClient#getTweetsByGeoCode(double,
	 * double, double, java.lang.String, placet.twitter.client.rest.ResultType,
	 * long, int)
	 */
	@Override
	// TODO => Try to process this in paralle with SpringRestTemplate
	public SearchResponse getTweetsByGeoCode(String searchContent, double latitude, double longitude, double radius,
			RadiusType rdType, String lang, ResultType resType, long sinceId, long maxId, int count)
					throws RestSearchRequestException
	{
		logger.debug("Start requesting tweets from twitter search endpoint with params => latitude[" + latitude
				+ "] longitude[" + longitude + "] radius[" + radius + "] lang[" + lang + "] resType[" + resType
				+ "] sinceId[" + sinceId + "] maxId[" + maxId + "] count[" + count + "]");
		SearchResponse responsePojo = null;

		try
		{
			final String bearerToken = TwitterOAuthBearerTokenBroker.requestSecurityToken();
			final MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("q", searchContent);
			params.add("geocode", latitude + "," + longitude + "," + radius + rdType.name().toLowerCase());
			params.add("lang", lang);
			params.add("result_type", resType.toString().toLowerCase());
			if (sinceId > 0)
			{
				params.add("since_id", String.valueOf(sinceId));
			}
			if (maxId > 0)
			{
				params.add("max_id", String.valueOf(maxId));
			}
			params.add("count", String.valueOf(count));

			logger.debug("Start calling the twitter endpoint");
			final long start = System.currentTimeMillis();
			final ClientResponse response = twitterEndpoint.queryParams(params)
					.header("Authorization", "Bearer " + bearerToken).get(ClientResponse.class);
			final long end = System.currentTimeMillis();
			logger.debug("End calling twitter endpoint. Time: " + (end - start));
			if (response.getStatus() != 200)
			{
				logger.error("The request was denied by the twitter endpoint => status [" + response.getStatus() + "]");
				throw new RestSearchRequestException("No valid request");
			}
			logger.debug("ResponseCode " + response.getStatus());
			responsePojo = response.getEntity(SearchResponse.class);
			logger.debug("Search endpoint result fetch count => " + responsePojo.getTweetList().size());
			response.close();

		}
		catch (final AuthenticationException e)
		{
			throw new RestSearchRequestException("This request was not authenticated", e);
		}
		logger.debug("End requesting tweets");

		return responsePojo;
	}

}
