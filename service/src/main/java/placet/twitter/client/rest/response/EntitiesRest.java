package placet.twitter.client.rest.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntitiesRest {

	@XmlElement(name = "hashtags")
	private List<HashTagRest> hashTags;

	public List<HashTagRest> getHashTags() {
		return hashTags;
	}

	public void setHashTags(List<HashTagRest> hashTags) {
		this.hashTags = hashTags;
	}

}
