package placet.twitter.client.rest.authentication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TwitterClientCredentialEncoder
{
	private static final Logger logger = LogManager.getLogger(TwitterClientCredentialEncoder.class.getName());
	private TwitterClientCredentialEncoder()
	{
	};

	public static String encode(String key, String secrete) throws UnsupportedEncodingException
	{
		logger.debug("Start encoding key/secrete credential");
		try
		{
			final String encodedConsumerKey = URLEncoder.encode(key, "UTF-8");
			final String encodedConsumerSecret = URLEncoder.encode(secrete, "UTF-8");

			final String fullKey = encodedConsumerKey + ":" + encodedConsumerSecret;
			final byte[] encodedBytes = Base64.getEncoder().encode((fullKey.getBytes()));
			logger.debug("End encoding key/secrete credentials");
			return new String(encodedBytes);
		}
		catch (final UnsupportedEncodingException e)
		{
			logger.error("Error during credential encoding");
			return new String();

		}

	}
}
