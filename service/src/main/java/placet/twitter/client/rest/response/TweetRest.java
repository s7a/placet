package placet.twitter.client.rest.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Arturooo
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetRest {

	@XmlElement(name = "id")
	private Long id;

	@XmlElement(name = "created_at")
	private String createdAtString;

	@XmlElement(name = "entities")
	private EntitiesRest entities;

	@XmlElement(name = "text")
	private String text;

	@XmlElement(name = "source")
	private String source;

	@XmlElement(name = "user")
	private UserRest user;

	@XmlElement(name = "retweet_count")
	private int retweetCount;

	@XmlElement(name = "favourites_count")
	private int favouritesCount;

	public int getFavouritesCount() {
		return favouritesCount;
	}

	public void setFavouritesCount(int favouritesCount) {
		this.favouritesCount = favouritesCount;
	}

	public int getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}

	public EntitiesRest getEntities() {
		return entities;
	}

	public void setEntities(EntitiesRest entities) {
		this.entities = entities;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public UserRest getUser() {
		return user;
	}

	public void setUser(UserRest user) {
		this.user = user;
	}

	public String getCreatedAtString() {
		return createdAtString;
	}

	public void setCreatedAtString(String createdAtString) {
		this.createdAtString = createdAtString;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
