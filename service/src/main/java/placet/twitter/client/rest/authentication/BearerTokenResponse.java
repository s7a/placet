package placet.twitter.client.rest.authentication;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BearerTokenResponse
{

	@XmlElement(name = "token_type")
	private String tokenType;

	@XmlElement(name = "access_token")
	private String accessToken;

	/**
	 * 
	 * @return The tokenType
	 */
	public String getTokenType()
	{
		return tokenType;
	}

	/**
	 * 
	 * @param tokenType
	 *            The token_type
	 */
	public void setTokenType(String tokenType)
	{
		this.tokenType = tokenType;
	}

	/**
	 * 
	 * @return The accessToken
	 */
	public String getAccessToken()
	{
		return accessToken;
	}

	/**
	 * 
	 * @param accessToken
	 *            The access_token
	 */
	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

}