package placet.twitter.client.rest.authentication;

import static placet.twitter.client.rest.RestClientConstants.TWITTER_APP_NAME;
import static placet.twitter.client.rest.RestClientConstants.TWITTER_GRANT_POST_KEY;
import static placet.twitter.client.rest.RestClientConstants.TWITTER_GRANT_POST_VALUE;
import static placet.twitter.client.rest.RestClientConstants.TWITTER_OAUTH_ENDPOINT;
import static placet.twitter.client.rest.RestClientConstants.TWITTER_RESPONSE_TOKEN_TYPE;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import placet.util.PlacetProperties;

/**
 * @author SynowiecA
 *
 */
public class TwitterOAuthBearerTokenBroker
{

	private final static Logger logger = LogManager.getLogger(TwitterOAuthBearerTokenBroker.class.getName());

	private static String bearerToken = null;

	private TwitterOAuthBearerTokenBroker() throws IllegalAccessException
	{
		throw new IllegalAccessException();
	};

	public static String requestSecurityToken() throws AuthenticationException
	{
		logger.debug("requestNewSecurityToken() start");
		if (bearerToken == null)
		{
			logger.debug("Bearer token is invalid, requesting a new token at Twitter endpoint");
			try
			{
				final String encodedClientCredentials = TwitterClientCredentialEncoder
						.encode(PlacetProperties.getAppKey(), PlacetProperties.getAppSecret());
				final ClientConfig clientConfig = new DefaultClientConfig();
				clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
				final Client client = Client.create(clientConfig);

				// TODO => Wire the filter to my logger
				client.addFilter(new LoggingFilter());

				final WebResource resource = client.resource(TWITTER_OAUTH_ENDPOINT);
				resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON);
				resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
				final MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
				formData.add(TWITTER_GRANT_POST_KEY, TWITTER_GRANT_POST_VALUE);

				final ClientResponse response = resource.header("User-Agent", TWITTER_APP_NAME)
						.header("Authorization", "Basic " + encodedClientCredentials)
						.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
						.post(ClientResponse.class, formData);

				logger.debug("Twitter OAuth respone => " + response);

				if (response.getStatus() != 200)
				{
					logger.error("Twitter endpoint could not authenticate the credentia l[" + encodedClientCredentials
							+ "]");
					throw new AuthenticationException("Error while requesting bearer token from Twitter");
				}

				final BearerTokenResponse bearerResponse = response.getEntity(BearerTokenResponse.class);

				if (bearerResponse.getTokenType().equals(TWITTER_RESPONSE_TOKEN_TYPE))
				{
					bearerToken = bearerResponse.getAccessToken();
				}

			}
			catch (final UnsupportedEncodingException e)
			{
				logger.error("The credentials[" + PlacetProperties.getAppKey() + " , " + PlacetProperties.getAppSecret()
						+ "] could not be encoded");
				throw new AuthenticationException("Client credentials could not be encoded", e);
			}
		}

		logger.debug("requestNewSecurityToken() end");
		return bearerToken;

	}

	public static void invalidateBearerToken()
	{
		if (bearerToken != null)
		{
			bearerToken = null;
		}
	}
}
