package placet.twitter.client.rest;

public class RestClientConstants
{

	private RestClientConstants()
	{
		throw new AssertionError();
	}

	public final static String TWITTER_APP_NAME = "placeTT";
	public final static String TWITTER_OAUTH_ENDPOINT = "https://api.twitter.com/oauth2/token";
	public final static String TWITTER_GRANT_POST_KEY = "grant_type";
	public final static String TWITTER_GRANT_POST_VALUE = "client_credentials";
	public final static String TWITTER_RESPONSE_TOKEN_TYPE = "bearer";

	public final static String TWITTER_ENDPOINT = "https://api.twitter.com/1.1/";
	public final static String JASON_SEARCH_TWEETS_PATH = "search/tweets.json";

}
