package placet.twitter.client.rest;

/**
 * @author SynowiecA
 *
 */
public enum RadiusType
{
	KM, MI

}
