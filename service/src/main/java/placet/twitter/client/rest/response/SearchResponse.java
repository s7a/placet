package placet.twitter.client.rest.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Arturooo
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResponse {

	@XmlElement(name = "statuses")
	private List<TweetRest> tweets;

	public List<TweetRest> getTweetList() {
		return tweets;
	}

	public void setStatuses(List<TweetRest> statuses) {
		this.tweets = statuses;
	}

	@Override
	public String toString() {
		return "SearchResponse [statuses=" + tweets != null ? tweets.toString() : tweets + "]";
	}

}
