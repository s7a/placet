package placet.twitter.client;

import java.util.List;

import placet.entity.info.twitter.Tweet;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.RestSearchRequestException;
import placet.twitter.client.rest.ResultType;
import placet.twitter.client.rest.response.SearchResponse;

/**
 * @author SynowiecA
 *
 */
public interface ITwitterClient {
	/**
	 * Returns a list of Tweets depending on the input parameters /**
	 * 
	 *
	 * @param searchContent
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @param rdType
	 * @param lang
	 * @param resType
	 * @param sinceId
	 * @param maxId
	 * @param count
	 * @return
	 * @throws RestSearchRequestException
	 */
	public SearchResponse getTweetsByGeoCode(String searchContent, double latitude, double longitude, double radius,
			RadiusType rdType, String lang, ResultType resType, long sinceId, long maxId, int count)
					throws RestSearchRequestException;
}
