package placet.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import placet.entity.Persistable;

public abstract class AbstractDAO <T extends Persistable> implements EntityController {
	
	
	private Class<T> entityClass;

	public AbstractDAO(Class<T> entityClass)
	{
		
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();
	
	protected abstract Logger getLogger();

	public void create(T entity)
	{
		getLogger().debug("Start creating entity");
		getEntityManager().persist(entity);
		getLogger().debug("End creating entity");
	}

	public void edit(T entity)
	{
		getLogger().debug("Start editig entity");
		getEntityManager().merge(entity);
		getLogger().debug("End editig entity");
	}

	public void remove(T entity)
	{
		
		getLogger().debug("Start removing entity");
		T delete = getEntityManager().merge(entity);
		getEntityManager().remove(delete);
		getLogger().debug("End removing entity");
		
		
	}

//	public T find(ModelKey id)
//	{
//		T retVal = getEntityManager().find(entityClass, id.getValue());
//
//		return retVal;
//	}

	protected T find(Object id)
	{
		getLogger().debug("Start finding entity with id => " +id);
		T retVal = getEntityManager().find(entityClass, id);
		getLogger().debug("End finding entity");

		return retVal;
	}

	protected Persistable find(Object id, Class<T> entity)
	{
		getLogger().debug("Start finding entity with id => " +id);
		return (Persistable) getEntityManager().find(entity, id);
	}

//	protected Persistable find(ModelKey id, Class<T> entity)
//	{
//		return (Persistable) getEntityManager().find(entity, id.getValue());
//	}

	public List<T> findAll()
	{
		getLogger().debug("Start finding all entities");
		javax.persistence.criteria.CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(
				entityClass);
		cq.select(cq.from(entityClass));
		getLogger().debug("End finding all entities");
		return getEntityManager().createQuery(cq).getResultList();
	}

	public List<T> findRange(int[] range)
	{
		getLogger().debug("Start finding entity in range =>" + range);
		javax.persistence.criteria.CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(
				entityClass);
		cq.select(cq.from(entityClass));
		javax.persistence.TypedQuery<T> q = getEntityManager().createQuery(cq);
		q.setMaxResults(range[1] - range[0] + 1);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	public long count()
	{
		getLogger().debug("Start counting entities");
		javax.persistence.criteria.CriteriaQuery<Long> cq = getEntityManager().getCriteriaBuilder().createQuery(
				Long.class);
		javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
		cq.select(getEntityManager().getCriteriaBuilder().count(rt));
		javax.persistence.TypedQuery<Long> q = getEntityManager().createQuery(cq);
		Long ret = q.getSingleResult();
		getLogger().debug("End counting entities, result => "+ ret);
		return ret == null ? 0 : ret;
	}

	protected void load(Set<? extends Persistable> entities)
	{
	   if (entities != null)
           {
               entities.size();
           }

	}


	protected void detach(Persistable entity)
	{
		if (entity != null)
			getEntityManager().detach(entity);
	}




	protected boolean isBlank(String string)
	{
		if (string != null && string.length() > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

}
