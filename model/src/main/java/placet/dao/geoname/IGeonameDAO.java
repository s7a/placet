package placet.dao.geoname;

import java.util.List;

import placet.entity.geoname.Geoname;

public interface IGeonameDAO {

	public void create(Geoname geoname);
	
	public void edit(Geoname geoname);
	
	public void remove(Geoname geoname);
		
	public List<Geoname> findAll();
	
	public long count();
	
}
