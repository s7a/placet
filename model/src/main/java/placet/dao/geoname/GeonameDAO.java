package placet.dao.geoname;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import placet.entity.geoname.Geoname;

@Repository
public class GeonameDAO extends AbstractGeonameDAO {
	
	private static final Logger logger = LogManager.getLogger(GeonameDAO.class.getName());

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public EntityManager getEntityManager() {
		return em;
	}
	
	public GeonameDAO(){
		super(Geoname.class);
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
