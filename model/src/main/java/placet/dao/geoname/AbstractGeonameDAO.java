package placet.dao.geoname;

import placet.dao.AbstractDAO;
import placet.entity.geoname.Geoname;

public abstract class AbstractGeonameDAO extends AbstractDAO<Geoname> implements IGeonameDAO {
	
	public AbstractGeonameDAO()
	{
		super(Geoname.class);
	}

	public AbstractGeonameDAO(Class<Geoname> entityClass)
	{
		super(entityClass);
	}

}
