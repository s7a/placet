package placet.dao.twitter.tweet;

import java.util.List;

import placet.entity.info.twitter.Tweet;

public interface ITweetDAO {
	
	public void create(Tweet tweet);
	
	public void edit(Tweet tweet);
	
	public void remove(Tweet tweet);
		
	public List<Tweet> findAll();
	
	public long count();

}
