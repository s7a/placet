package placet.dao.twitter.tweet;

import placet.dao.AbstractDAO;
import placet.entity.info.twitter.Tweet;

public abstract class AbstractTweetDAO extends AbstractDAO<Tweet> implements ITweetDAO {

	public AbstractTweetDAO(){
		super(Tweet.class);
	}
	
	public AbstractTweetDAO(Class<Tweet> entityClass) {
		super(entityClass);
	}

}
