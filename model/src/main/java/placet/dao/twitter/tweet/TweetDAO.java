package placet.dao.twitter.tweet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import placet.entity.info.twitter.Tweet;

@Repository
public class TweetDAO extends AbstractTweetDAO {
	
	private static final Logger logger = LogManager.getLogger(TweetDAO.class.getName());

	@PersistenceContext
	private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public TweetDAO(){
		super(Tweet.class);
	}
	
	/* We want the to remove the Tweet but keep the HasTag
	 * 
	 * @see placet.dao.AbstractDAO#remove(placet.entity.Persistable)
	 */ 
	@Override
	public void remove(Tweet tweet){
		tweet.removeHashTah();
		Tweet delete = em.merge(tweet);
		em.remove(delete);
		
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

}
