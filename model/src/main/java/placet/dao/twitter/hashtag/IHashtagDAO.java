package placet.dao.twitter.hashtag;

import java.util.Date;
import java.util.List;

import placet.entity.info.twitter.HashTag;

public interface IHashtagDAO
{

	public void create(HashTag hashtag);

	public void edit(HashTag hashtag);

	public void remove(HashTag hashtag);

	public List<HashTag> findAll();

	public long count();

	public List<HashTag> findByGeoLocationAndDateTime(double longitude, double latitude, Date currentTimeMinusDelta);

	public void batchInsert(List<HashTag> hashTagList);

}
