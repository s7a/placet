package placet.dao.twitter.hashtag;

import placet.dao.AbstractDAO;
import placet.entity.info.twitter.HashTag;

public abstract class AbstractHashtagDAO extends AbstractDAO<HashTag> implements IHashtagDAO {
	
	
	public AbstractHashtagDAO(){
		super(HashTag.class);
	}

	public AbstractHashtagDAO(Class<HashTag> entityClass) {
		super(entityClass);
	}
	
	
	
	

}
