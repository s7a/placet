package placet.dao.twitter.hashtag;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import placet.entity.info.twitter.HashTag;

@Repository
public class HashTagDAO extends AbstractHashtagDAO {

	private static final Logger logger = LogManager.getLogger(HashTagDAO.class.getName());

	private static final int BATCH_SIZE = 20;

	@PersistenceContext
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public HashTagDAO() {
		super(HashTag.class);

	}

	@Override
	protected Logger getLogger() {
		return logger;
	}

	@Override
	public List<HashTag> findByGeoLocationAndDateTime(double longitude, double latitude, Date currentTimeMinusDelta) {
		final TypedQuery<HashTag> query = em.createNamedQuery("findHashTagsByGeoLocAndCreatedAt", HashTag.class);
		query.setParameter("longitude", new Double(longitude));
		query.setParameter("latitude", new Double(latitude));
		query.setParameter("currentTimeMinusDelta", currentTimeMinusDelta);

		final List<HashTag> res = query.getResultList();
		return res;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void batchInsert(List<HashTag> hashTagList) {

		for (int i = 0; i < hashTagList.size(); i++) {
			em.persist(hashTagList.get(i));

			if (i % BATCH_SIZE == 0) {
				em.flush();
				em.clear();
			}

		}

	}

}
