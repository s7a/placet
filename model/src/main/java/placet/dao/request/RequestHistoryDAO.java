package placet.dao.request;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import placet.entity.request.RequestHistory;

@Repository
public class RequestHistoryDAO extends AbstractRequestHistoryDAO {

	private static final Logger logger = LogManager.getLogger(RequestHistoryDAO.class.getName());

	@PersistenceContext
	private EntityManager em;

	public RequestHistoryDAO() {
		super(RequestHistory.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	protected Logger getLogger() {
		return logger;
	}
}
