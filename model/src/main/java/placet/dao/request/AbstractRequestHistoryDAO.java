package placet.dao.request;

import placet.dao.AbstractDAO;
import placet.entity.request.RequestHistory;

public abstract class AbstractRequestHistoryDAO extends AbstractDAO<RequestHistory>implements IRequestHistoryDAO {

	public AbstractRequestHistoryDAO() {
		super(RequestHistory.class);
	}

	public AbstractRequestHistoryDAO(Class<RequestHistory> entityClass) {
		super(entityClass);

	}
}
