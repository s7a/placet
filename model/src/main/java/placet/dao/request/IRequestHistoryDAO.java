package placet.dao.request;

import java.util.List;

import placet.entity.request.RequestHistory;

public interface IRequestHistoryDAO {

	public void create(RequestHistory hist);

	public void edit(RequestHistory hist);

	public void remove(RequestHistory hist);

	public List<RequestHistory> findAll();

	public long count();

}
