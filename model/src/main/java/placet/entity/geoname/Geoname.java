package placet.entity.geoname;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import placet.entity.Persistable;

@Entity
@Table(name="Geoname")
public class Geoname implements Persistable{
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="ID")
	private Integer id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="LONGITUDE")
	private Double longitude;
	
	@Column(name="LATITUDE")
	private Double latitude;
	
	@Column(name="POPULATION")
	private BigInteger population;
	
	
	
	

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	/**
	 * Warning - this method won't work in the case the id fields are not set
	 */
	@Override
	public boolean equals(Object object)
	{
		if (!(object instanceof Geoname))
		{
			return false;
		}
		Geoname other = (Geoname) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Geoname [id=" + id + ", name=" + name + ", longitude="
				+ longitude + ", latitude=" + latitude + ", population="
				+ population + "]";
	}
	
	
	
	

}
