package placet.entity.info.twitter;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import placet.entity.Persistable;

@Entity
@Table(name = "TWEET")
public class Tweet implements Persistable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "TEXT_CONTENT")
	private String textContent;

	@Column(name = "TWITTER_ID")
	private Long twitterID;

	@Column(name = "USER_NAME")
	private String userName;

	@Column(name = "SCREEN_NAME")
	private String screenName;

	@Column(name = "RETWEET_COUNT")
	private int retweetCount;

	@Column(name = "favourites_count")
	private int favouritesCount;

	@Column(name = "PROFILE_IMAGE_URL")
	private String profileImageUrl;

	@Column(name = "CREATED_AT")
	private Date createdAt;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
			CascadeType.REMOVE })
	@JoinColumn(name = "HASH_TAG_ID")
	@JsonBackReference
	private HashTag hashTag;

	public String getTextContent()
	{
		return textContent;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public int getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}

	public int getFavouritesCount() {
		return favouritesCount;
	}

	public void setFavouritesCount(int favouritesCount) {
		this.favouritesCount = favouritesCount;
	}

	public void setTextContent(String textContent)
	{
		this.textContent = textContent;
	}

	public Integer getId()
	{
		return id;
	}

	public HashTag getHashTag()
	{
		return hashTag;
	}

	public void setHashTag(HashTag hashTag)
	{
		if (!hashTag.getTweets().contains(this))
		{
			hashTag.getTweets().add(this);
		}
		this.hashTag = hashTag;
	}

	public void removeHashTah()
	{
		if (this.hashTag != null)
		{
			hashTag.getTweets().remove(this);
			hashTag = null;
		}
	}

	public Long getTwitterID()
	{
		return twitterID;
	}

	public void setTwitterID(Long twitterID)
	{
		this.twitterID = twitterID;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getProfileImageUrl()
	{
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl)
	{
		this.profileImageUrl = profileImageUrl;
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("Tweet [id=");
		builder.append(id);
		builder.append(", textContent=");
		builder.append(textContent);
		builder.append(", twitterID=");
		builder.append(twitterID);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", profileImageUrl=");
		builder.append(profileImageUrl);
		builder.append(", createdAt=");
		builder.append(createdAt);
		builder.append(", hashTag=");
		builder.append(hashTag);
		builder.append("]");
		return builder.toString();
	}

}
