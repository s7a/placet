package placet.entity.info.twitter;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import placet.entity.Persistable;

@NamedQuery(name = "findHashTagsByGeoLocAndCreatedAt", query = "Select hash from HashTag hash  where hash.longitude = :longitude and hash.latitude = :latitude and hash.createdAt >= :currentTimeMinusDelta ORDER BY hash.occurrence DESC")

@Entity
@Table(name = "HASH_TAG")
public class HashTag implements Persistable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "OCCURRENCE")
	private Integer occurrence;

	@Column(name = "CREATED_AT")
	private Date createdAt;

	@Column(name = "LONGITUDE")
	private Double longitude;

	@Column(name = "LATITUDE")
	private Double latitude;

	@OneToMany(mappedBy = "hashTag", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH, CascadeType.REMOVE })
	@JsonManagedReference
	private List<Tweet> tweets;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getId()
	{
		return id;
	}

	public Integer getOccurrence()
	{
		return occurrence;
	}

	public void setOccurrence(Integer occurrence)
	{
		this.occurrence = occurrence;
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public List<Tweet> getTweets()
	{
		return tweets;
	}

	public Double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}

	public Double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	public void setTweets(List<Tweet> tweets)
	{
		this.tweets = tweets;
		this.tweets.stream().filter(tweet -> tweet.getHashTag() != this).forEach(tweet -> tweet.setHashTag(this));

	}

	public void removeTweet(Tweet tweet)
	{
		if (this.getTweets().contains(tweet))
		{
			this.getTweets().remove(tweet);
		}
	}

	@Override
	public String toString()
	{
		return "HashTag [id=" + id + ", name=" + name + ", occurrence=" + occurrence + ", createdAt=" + createdAt
				+ ", tweets=" + tweets + "]";
	}

}
