package placet.entity;

import java.io.Serializable;

public interface Persistable  extends Serializable
{
	/**
	 * Warning - this method won't work in the case the id fields are not set
	 */
	public boolean equals(Object object);


}
