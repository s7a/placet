﻿ create table geoname (
         geonameid       int,
         name            varchar(200),
         asciiname        varchar(200),
         alternatenames  varchar(6000),
         latitude        float,
         longitude       float,
         fclass  char(1),
         fcode   varchar(10),
         country varchar(2),
         cc2 varchar(60),
         admin1  varchar(20),
         admin2  varchar(80),
         admin3  varchar(20),
         admin4  varchar(20),
         population      bigint,
         elevation       int,
         gtopo30         int,
         timezone varchar(40),
         moddate         date
 );

 -- Not yet used
 create table alternatename (
         alternatenameId  int,
         geonameid          int,
         isoLanguage        varchar(7),
         alternateName     varchar(200),
         isPreferredName      boolean,
         isShortName    boolean,
         isColloquial    boolean,
         isHistoric    boolean
 );
 
 
 -- Broken File
 CREATE TABLE countryinfo (
      iso_alpha2 char(2),
      iso_alpha3 char(3),
      iso_numeric integer,
      fips_code character varying(3),
      name character varying(200),
      capital character varying(200),
      areainsqkm double precision,
      population integer,
      continent char(2),
      languages character varying(200),
      currency char(3),
      geonameId int,
      neighbours char(3),
      equivalentFipsCode char(3)	
      
 );

 
 ALTER TABLE ONLY alternatename
     ADD CONSTRAINT pk_alternatenameid PRIMARY KEY (alternatenameid);
 
 ALTER TABLE ONLY geoname
     ADD CONSTRAINT pk_geonameid PRIMARY KEY (geonameid);
 
 ALTER TABLE ONLY countryinfo
     ADD CONSTRAINT pk_iso_alpha2 PRIMARY KEY (iso_alpha2);


-- Adjust the patch to the .txt files
     copy geoname (geonameid,name,asciiname,alternatenames,latitude,longitude,fclass,fcode,country,cc2, admin1,admin2,admin3,admin4,population,elevation,gtopo30,timezone,moddate) from 'C:\Dev\Projects\PlaceT\model\src\main\resources\geoname\cities15000.txt' null as '';
     -- Broken
     copy countryInfo  (iso_alpha2,iso_alpha3,iso_numeric,fips_code,name,capital,areaInSqKm,population,continent,languages,currency,geonameId) from 'C:\Dev\Projects\PlaceT\model\src\main\resources\geoname\countryInfo.txt' null as '';

 