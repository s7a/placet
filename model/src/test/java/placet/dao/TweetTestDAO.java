package placet.dao;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.twitter.hashtag.HashTagDAO;
import placet.dao.twitter.hashtag.IHashtagDAO;
import placet.dao.twitter.tweet.ITweetDAO;
import placet.dao.twitter.tweet.TweetDAO;
import placet.entity.info.twitter.HashTag;
import placet.entity.info.twitter.Tweet;

/**
 * @author artursynowiec
 *
 *         This test class will cover tests for the Tweet entity
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/model_context_test.xml")
public class TweetTestDAO
{

	@Autowired
	ITweetDAO tweetDao;

	@Autowired
	IHashtagDAO hashTagDAO;

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	@Transactional
	public void testDataIsInDB()
	{
		final List<Tweet> tweets = tweetDao.findAll();
		Assert.assertNotEquals(0, tweets.size());

		Assert.assertFalse(tweets.stream().filter(t -> t.getHashTag() == null).findAny().isPresent());
	}

	@Test
	@Transactional
	public void testAddTweet()
	{

		final Tweet testTweet = new Tweet();
		final String profileImageUrl = "http://JUNIT";
		final String textContent = "junit_contetn";
		final Long twitterID = 1000000001L;
		final String userName = "junitJoe";

		testTweet.setProfileImageUrl(profileImageUrl);
		testTweet.setTextContent(textContent);
		testTweet.setTwitterID(twitterID);
		testTweet.setUserName(userName);

		final HashTag testHashTag = new HashTag();
		testHashTag.setCreatedAt(new Date());
		final String hashTagName = "#hashi";
		testHashTag.setName(hashTagName);
		testHashTag.setTweets((Arrays.asList(testTweet)));
		testTweet.setHashTag(testHashTag);

		tweetDao.create(testTweet);

		final List<Tweet> tweets = tweetDao.findAll();

		Assert.assertTrue(tweets.stream()
				.filter(t -> t.getTextContent().equals(textContent) && t.getProfileImageUrl().equals(profileImageUrl)
						&& t.getUserName().equals(userName) && t.getTwitterID() == twitterID
						&& t.getHashTag().getName().equals(hashTagName))
				.findAny().isPresent());

	}

	@Test
	@Transactional
	public void testAlterData()
	{

		List<Tweet> tweets = tweetDao.findAll();
		final String testName = "Junit";
		tweets.get(0).setTextContent(testName);

		tweetDao.edit(tweets.get(0));

		tweets = tweetDao.findAll();

		Assert.assertTrue(tweets.stream().filter(t -> t.getTextContent().equals(testName)).findAny().isPresent());
	}

	@Test
	@Transactional
	public void testDeleteData()
	{

		List<Tweet> tweets = tweetDao.findAll();
		final int tweetSizeBevoreDelete = tweets.size();
		List<HashTag> hashTags = hashTagDAO.findAll();
		final int hasTagSizeBevoreDelete = hashTags.size();

		tweetDao.remove(tweets.get(0));

		tweets = tweetDao.findAll();
		hashTags = hashTagDAO.findAll();

		Assert.assertNotEquals(tweetSizeBevoreDelete, tweets.size());
		Assert.assertEquals(hasTagSizeBevoreDelete, hashTags.size());
	}

}
