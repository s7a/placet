package placet.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import placet.dao.request.IRequestHistoryDAO;
import placet.entity.request.RequestHistory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/model_context_test.xml")
public class RequestHistoryDAOTest {

	@Autowired
	IRequestHistoryDAO dao;

	@Test
	@Transactional
	public void testDataIsInDB() {
		final List<RequestHistory> list = dao.findAll();
		Assert.assertNotEquals(0, list.size());

	}

	@Test
	@Transactional
	public void testCreateNewRequestHistory() {

		final String ip = "100.0.0.7";
		final String city = "Trier";
		final Date reqDate = new Date();
		final Long responseTime = 300L;
		final String servedBy = "DB";

		final RequestHistory newHist = new RequestHistory();
		newHist.setCity(city);
		newHist.setDate(reqDate);
		newHist.setIp(ip);
		newHist.setServedBy(servedBy);
		newHist.setResponseTime(responseTime);

		dao.create(newHist);

		final List<RequestHistory> list = dao.findAll();

		Assert.assertTrue(list.stream()
				.filter(hist -> hist.getIp().equals(ip) && hist.getDate() == reqDate && hist.getCity().equals(city)
						&& hist.getResponseTime() == responseTime && hist.getServedBy().equals(servedBy))
				.findAny().isPresent());

	}

	@Test
	@Transactional
	public void testAlterData() {
		List<RequestHistory> list = dao.findAll();
		final RequestHistory entityOne = list.get(0);
		final String testCity = "JUNIT";
		entityOne.setCity(testCity);

		dao.edit(entityOne);

		list = dao.findAll();
		Assert.assertTrue(list.stream().filter(hist -> hist.getCity().equals(testCity)).findAny().isPresent());

	}

	@Test
	@Transactional
	public void testDeleteRequestHistory() {

		List<RequestHistory> histList = dao.findAll();
		final int sizeBevoreDelete = histList.size();
		dao.remove(histList.get(0));

		histList = dao.findAll();
		Assert.assertNotEquals(sizeBevoreDelete, histList.size());

	}

}
