package placet.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.twitter.hashtag.IHashtagDAO;
import placet.dao.twitter.tweet.ITweetDAO;
import placet.entity.info.twitter.HashTag;
import placet.entity.info.twitter.Tweet;

/**
 * @author artursynowiec
 *
 *         This test class will cover tests for the HashTag entity
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/model_context_test.xml")
public class HashTagTestDAO
{

	@Autowired
	IHashtagDAO hashTagDAO;

	@Autowired
	ITweetDAO tweetDAO;

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	@Transactional
	public void testDataIsInDB()
	{
		final List<HashTag> list = hashTagDAO.findAll();
		Assert.assertNotEquals(0, list.size());

	}

	@Test
	@Transactional
	public void testTweetsAreAvailable()
	{
		final List<HashTag> list = hashTagDAO.findAll();
		final HashTag hashTag = list.get(0);
		final List<Tweet> tweets = hashTag.getTweets();
		Assert.assertNotNull(tweets);
		Assert.assertNotEquals(0, tweets.size());

	}

	@Test
	@Transactional
	public void testCreateNewHashTag()
	{

		final String hashTagName = "TestHash";
		final String tweetContent = "TestTweet";
		final Integer occurrence = 3;

		final HashTag newHashTag = new HashTag();
		newHashTag.setName(hashTagName);
		final Date now = new Date();
		newHashTag.setCreatedAt(now);
		newHashTag.setOccurrence(occurrence);

		final Tweet testTweet = new Tweet();
		testTweet.setTextContent(tweetContent);
		final List<Tweet> tweets = new ArrayList<>();
		tweets.add(testTweet);

		newHashTag.setTweets(tweets);
		testTweet.setHashTag(newHashTag);

		hashTagDAO.create(newHashTag);

		final List<HashTag> list = hashTagDAO.findAll();

		Assert.assertTrue(list.stream().filter(hashTag -> hashTag.getName().equals(hashTagName)
				&& hashTag.getCreatedAt() == now && hashTag.getOccurrence() == occurrence).findAny().isPresent());

		Assert.assertNotNull(
				list.stream().filter(hashTag -> hashTag.getTweets().size() > 0 && hashTag.getName().equals(hashTagName))
						.findAny().isPresent());

	}

	@Test
	@Transactional
	public void testAlterData()
	{
		List<HashTag> list = hashTagDAO.findAll();
		final HashTag entityOne = list.get(0);
		final String testName = "JUNIT";
		entityOne.setName(testName);
		final List<Tweet> tweets = entityOne.getTweets();
		tweets.forEach(t -> t.setTextContent(testName));

		hashTagDAO.edit(entityOne);

		list = hashTagDAO.findAll();
		Assert.assertTrue(list.stream().filter(
				h -> h.getTweets().stream().filter(t -> t.getTextContent().equals(testName)).findAny().isPresent())
				.filter(h -> h.getName().equals(testName)).findAny().isPresent());

	}

	@Test
	@Transactional
	public void testDeleteHashTag()
	{
		List<HashTag> hashTags = hashTagDAO.findAll();
		List<Tweet> tweets = tweetDAO.findAll();
		final int tweetSizeBevoreDelete = tweets.size();
		final int hashTagsizeBevoreDelete = hashTags.size();
		hashTagDAO.remove(hashTags.get(0));

		hashTags = hashTagDAO.findAll();
		Assert.assertNotEquals(hashTagsizeBevoreDelete, hashTags.size());

		tweets = tweetDAO.findAll();
		Assert.assertNotEquals(tweetSizeBevoreDelete, tweets.size());

	}

	@Test
	@Transactional
	public void testBulkInsert()
	{
		List<HashTag> list = new ArrayList<>();
		for (int i = 0; i < 100; i++)
		{
			final HashTag h = new HashTag();
			list.add(h);
		}
		final long start = System.currentTimeMillis();
		hashTagDAO.batchInsert(list);
		final long end = System.currentTimeMillis();
		System.out.println("Batch insert in = >" + (end - start));

		list = hashTagDAO.findAll();
		Assert.assertTrue(list.size() > 100);
	}

}
