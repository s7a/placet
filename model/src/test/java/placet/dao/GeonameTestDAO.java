package placet.dao;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import placet.dao.geoname.GeonameDAO;
import placet.entity.geoname.Geoname;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:/model_context_test.xml")
public class GeonameTestDAO
{

	@Autowired
	GeonameDAO dao;

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	@Transactional
	public void testDataIsInDB()
	{
		final List<Geoname> list = dao.findAll();
		Assert.assertNotEquals(0, list.size());

	}

	@Test
	@Transactional
	public void testRmove()
	{
		List<Geoname> list = dao.findAll();
		final Geoname entryOne = list.get(0);
		final String nameOfEntryOne = entryOne.getName();

		dao.remove(entryOne);
		list = dao.findAll();
		final Geoname entryOneAfterRemove = list.get(0);
		final String nameOfEntryOneAfterRemove = entryOneAfterRemove.getName();

		Assert.assertNotEquals(nameOfEntryOne, nameOfEntryOneAfterRemove);

	}

	@Test
	@Transactional
	public void testAlter()
	{
		List<Geoname> list = dao.findAll();
		Geoname entryOne = list.get(0);
		dao.getEntityManager().detach(entryOne);
		entryOne.setName("JUNIT");
		dao.getEntityManager().merge(entryOne);
		list = dao.findAll();
		entryOne = list.get(0);

		Assert.assertEquals(entryOne.getName(), "JUNIT");

	}

	@Test
	@Transactional
	public void testAdd()
	{
		final Geoname newEntry = new Geoname();
		newEntry.setName("Test");
		newEntry.setLatitude(-55D);
		newEntry.setLatitude(-3D);

		dao.create(newEntry);

		final List<Geoname> list = dao.findAll();
		Assert.assertEquals(list.size(), 3);

	}

}
