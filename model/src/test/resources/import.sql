-- Geoname
INSERT into Geoname (id,latitude,longitude, name, population) VALUES (1,37.774929500000000000,-122.419415500000010000, 'San Francisco',1000000);
INSERT into Geoname (id,latitude,longitude, name, population) VALUES (2,56,889, 'Trier', 500000);
-- HashTag
INSERT into HASH_TAG (ID,NAME,CREATED_AT,LONGITUDE,LATITUDE) VALUES (1,'Geilor',current_date,53.3498053,-6.260309699999993);
INSERT into HASH_TAG (ID,NAME,CREATED_AT,LONGITUDE,LATITUDE) VALUES (2,'DublinIsGreat',current_date,53.3498053,-6.260309699999993);
--Tweets
INSERT into TWEET (ID,TEXT_CONTENT,HASH_TAG_ID,TWITTER_ID,USER_NAME,PROFILE_IMAGE_URL) VALUES (1,'This is the test_Tweet for Geilor',1,1212313221233, 'Sven', 'http:\\sdfsdf');
INSERT into TWEET (ID,TEXT_CONTENT,HASH_TAG_ID,TWITTER_ID,USER_NAME,PROFILE_IMAGE_URL) VALUES (2,'This is the  DublinIsGreat tweet',1,12312738712387), 'Gerri', 'http:\\sdfsdf');
--RequestHistory
INSERT into REQUEST_HISTORY (ID,REQ_DATE,IP,CITY,RESPONSE_TIME,SERVED_BY) VALUES (1,current_date,'100.0.0.7','Dublin',3085, 'DB');