package placet;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "placet.twitter.search.*", "placet.twitter.client", "placet.dao.twitter",
		"placet.dao.request" })
@PropertySource("classpath:application.properties")
public class PlacetTestConfiguration {

}
