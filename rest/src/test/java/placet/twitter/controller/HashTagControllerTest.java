package placet.twitter.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import placet.PlacetTestConfiguration;
import placet.twitter.search.ITwitterHashTagProzessorWrapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { MockServletContext.class, PlacetTestConfiguration.class })
@WebAppConfiguration
public class HashTagControllerTest
{

	public static final String VALID_REQUEST_URL = "/hashtags?content=124&lat=124&lon=345&rd=50&lang=en&city=Dublin&count=10";
	private MockMvc mvc;

	@Autowired
	@Qualifier("DB")
	ITwitterHashTagProzessorWrapper wrapper;

	@Before
	public void setUp() throws Exception
	{
		mvc = MockMvcBuilders.standaloneSetup(new HashTagController(wrapper)).build();
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testPositive() throws Exception
	{
		mvc.perform(MockMvcRequestBuilders.get(VALID_REQUEST_URL).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
