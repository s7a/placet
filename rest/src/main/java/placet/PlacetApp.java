package placet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ContextConfiguration;

@SpringBootApplication
// @ContextConfiguration(classes = PlacetConfiguration.class)
public class PlacetApp
{
	public static void main(String[] args)
	{
		SpringApplication.run(PlacetApp.class, args);
	}

}
