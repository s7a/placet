package placet;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@ImportResource("classpath:/model_context.xml")
@EnableAsync
public class PlacetConfiguration
{

}
