package placet.twitter.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import placet.entity.info.twitter.HashTag;
import placet.twitter.client.rest.RadiusType;
import placet.twitter.client.rest.ResultType;
import placet.twitter.search.HashTagProzessotException;
import placet.twitter.search.ITwitterHashTagProzessorWrapper;
import placet.twitter.search.db.TwitterHashTagDbProzessor;

// TODO => move the port to props file
@CrossOrigin(origins = "http://localhost:63342")
@RestController
public class HashTagController
{

	private static Logger logger = LogManager.getLogger(TwitterHashTagDbProzessor.class.getName());
	private final ITwitterHashTagProzessorWrapper hashTagProzessor;

	@Autowired
	public HashTagController(@Qualifier("DB") ITwitterHashTagProzessorWrapper hashTagProzessor)
	{
		this.hashTagProzessor = hashTagProzessor;
	}

	@RequestMapping("/hashtags")
	public List<HashTag> greeting(@RequestParam(value = "content", defaultValue = "") String searchContent,
			@RequestParam(value = "lat", required = true) double latitude,
			@RequestParam(value = "lon", required = true) double longitude,
			@RequestParam(value = "rd", defaultValue = "20.0") double radius,
			@RequestParam(value = "lang", defaultValue = "en") String lang,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "count", defaultValue = "5") int releventHashTagCount,
			final HttpServletRequest request)
	{

		logger.debug("/hashtags Endpoint hit by => " + request.getRemoteAddr());
		final long reqStart = System.currentTimeMillis();
		List<HashTag> hashTags = null;
		try
		{
			hashTags = hashTagProzessor.fetchMostRelevantHashTags(searchContent, latitude, longitude, radius,
					RadiusType.KM, lang, ResultType.RECENT, releventHashTagCount, city, request.getRemoteAddr());
		}
		catch (final HashTagProzessotException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final long reqEnd = System.currentTimeMillis();

		logger.debug("Request from [" + request.getRemoteAddr() + "] served in " + (reqEnd - reqStart) + " milliseconds");
		return hashTags;
	}

}
