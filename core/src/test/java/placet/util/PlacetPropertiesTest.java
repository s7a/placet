package placet.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class PlacetPropertiesTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void loadPropertiesTest()
	{
		PlacetProperties.load();
	}

	@Test
	public void getPropertiesTest()
	{
		Assert.assertEquals("placeT", PlacetProperties.getAppName());
	}

	@Test(expected = IllegalStateException.class)
	public void getMissingCriticalTest()
	{
		PlacetProperties.get("Not_Existing_property", true);
	}

	@Test
	public void getMissingPropertiesTest()
	{
		Assert.assertNull(PlacetProperties.get("Not_Existing_property", false));
	}

}
