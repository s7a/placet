package placet.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import placet.entity.info.twitter.Tweet;

public class PlacetUtils
{
	private static Logger logger = LogManager.getLogger(PlacetUtils.class.getName());

	private PlacetUtils()
	{
		throw new IllegalAccessError();
	}

	public static void sortTweetsByDate(List<Tweet> tweetList)
	{
		Collections.sort(tweetList, new Comparator<Tweet>()
		{
			@Override
			public int compare(Tweet o1, Tweet o2)
			{
				return o2.getCreatedAt().compareTo(o1.getCreatedAt());
			}
		});

	}

	public static void sortTweetsByPopularity(List<Tweet> tweetList)
	{
		// TODO => ...
	}

	public static <T> List<T> getSubList(List<T> list, int desiredElementsToFetch)
	{

		int returnCount;
		if (list.size() < desiredElementsToFetch)
		{
			logger.info(
					"The size [" + list.size() + "] of the List is smaller than the requested desiredElementsToFetch["
							+ desiredElementsToFetch + "]");
			returnCount = list.size();
		}
		else
		{
			returnCount = desiredElementsToFetch;
		}

		return list.subList(0, returnCount);

	}

	public static Date convertLdtToDate(LocalDateTime ldt)
	{
		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

}
