package placet.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PlacetProperties
{
	private final static Logger logger = LogManager.getLogger(PlacetProperties.class.getName());

	private static Properties properties = null;

	private final static String PROP_FILE = "placet.properties";

	private interface Props
	{

		String APP_NAME = "app.name";
		String LOG_PATH = "log.path";
		String APP_KEY = "app.key";
		String APP_SECRET = "app.secret";
		String TWEET_REQEST_ROUND = "tweet.reqest.round";
		String CACHE_FRESH_TIMEOUT = "cache.fresh.timeout";
		String CACHE_OLD_TIMEOUT = "cache.old.timeout";
		String SAVED_TWEET_PER_HASHTAG_NUMBER = "saved.tweet.per.hashtag.number";

	}

	private PlacetProperties() throws IllegalAccessException
	{
		throw new IllegalAccessException();
	};

	public static void reloadFromExternal()
	{

		// TODO => later we would love to have an option to reload the
		// properties from an external file location
	}

	public static void load()
	{

		logger.debug("Loading properties file[" + PROP_FILE + "]");
		final InputStream is = PlacetProperties.class.getClassLoader().getResourceAsStream(PROP_FILE);
		properties = new Properties();
		try
		{
			properties.load(is);
		}
		catch (final IOException e)
		{
			logger.error("Error loading properties file with name " + PROP_FILE);
			throw new IllegalStateException("Error loading properties file with name " + PROP_FILE, e);
		}
		logger.debug("Properties loaded successfully");

	}

	public static String get(String name, boolean critical)
	{
		if (properties == null)
		{
			load();
		}
		final String value = properties.getProperty(name);
		if (critical && value == null)
		{
			throw new IllegalStateException(
					"Critical property [" + name + "] name could not be found in property file [" + PROP_FILE + "]");
		}
		else if (value == null)
		{
			logger.warn("Property [" + name + "] name could not be found in property file [" + PROP_FILE
					+ "] - default or null used");
		}
		return value;
	}

	public static String getAppName()
	{
		return get(Props.APP_NAME, false);
	}

	public static String getLogPath()
	{
		return get(Props.LOG_PATH, false);
	}

	public static String getAppKey()
	{
		return get(Props.APP_KEY, true);
	}

	public static String getAppSecret()
	{
		return get(Props.APP_SECRET, true);
	}

	public static int getTweetReqestRound()
	{
		final String prop = get(Props.TWEET_REQEST_ROUND, false);
		if (prop != null)
		{
			return Integer.valueOf(prop);
		}
		else
		{
			return 10;
		}

	}

	public static long getCacheFreshTimeout()
	{
		final String prop = get(Props.CACHE_FRESH_TIMEOUT, false);
		if (prop != null)
		{
			return Long.valueOf(prop);
		}
		else
		{
			return 60;
		}
	}

	public static long getCacheOldTimeout()
	{
		final String prop = get(Props.CACHE_OLD_TIMEOUT, false);
		if (prop != null)
		{
			return Long.valueOf(prop);
		}
		else
		{
			return 480;
		}
	}

	public static int getSavedTweetsPerHashTagNumber()
	{
		final String prop = get(Props.SAVED_TWEET_PER_HASHTAG_NUMBER, false);
		if (prop != null)
		{
			return Integer.valueOf(prop);
		}
		else
			return 10;
	}

}
